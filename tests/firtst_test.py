import pytest


def add(num1: int, num2: int):
    return num1 + num2


def test_add_with_positive_nums():
    assert add(3, 2) == 5


def test_add_with_negative_nums():
    assert add(3, 2) == 6, f'Expected result: 6, actual result {add(3, 2)}'
